package br.com.testeAPI.api.steps.Breeds;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.junit.Assert;

public class ValidarExcessoes {

    public RequestSpecification request;
    public Response response;

    @Given("que acessei a API Thecat")
    public void que_acessei_a_API_Thecat() throws Throwable {
        request  = RestAssured.given().relaxedHTTPSValidation();
    }

    @When("pesquisar pela raca abys")
    public void acessar_a_raca_abys() throws Throwable {
        response = request.when().get("/breeds/abys").prettyPeek();
    }

    @Then("validar os dados solicitados")
    public void vou_validar_os_dados_solicitados() throws Throwable {
        Assert.assertEquals(response.jsonPath().get("name"), "Abyssinian");
        Assert.assertEquals(response.jsonPath().get("origin"), "Egypt");
        Assert.assertEquals(response.jsonPath().get("life_span"), "14 - 15");
        Assert.assertEquals((response.jsonPath().getInt("adaptability")), 5);
    }

    @When("consultar por uma raca inexistente")
    public void acessar_a_raca_SRD() throws Throwable {
        response = request.when().get("/breeds/SRD").prettyPeek();
    }

    @Then("validar status mas nao retorna dados")
    public void vou_validar_status_mas_nao_retorna_dados() throws Throwable {
        Assert.assertTrue(response.statusCode() == 200);
        Assert.assertFalse(response.asString().contains("id"));
        Assert.assertEquals(response.asString(),"{}");
    }
}
