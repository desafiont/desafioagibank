package br.com.testeAPI.api.steps.Favourites;


import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.junit.Assert;

public class ValidarDelete {

    public RequestSpecification requestPost;
    public RequestSpecification requestDelete;
    public Response responsePost;
    public Response responseDelete;
    public Integer idPost;

    @Given("que acessei exclusao de Favorito")
    public void que_acessei_exclusao_de_Favorito () throws Throwable {
        //CRIANDO REGISTRO PARA USAR NO DELETE
        requestPost = RestAssured.given().header("x-api-key", "DEMO-API-KEY").
                contentType("application/json").body("{\n" +
                "  \"image_id\": \"soneira37\" ,\n" +
                "  \"sub_id\": \"your-user-1234\"\n" +
                "}").relaxedHTTPSValidation();

        responsePost = requestPost.when().post("/favourites");
        idPost = responsePost.jsonPath().get("id");

        Thread.sleep(5000);

        //EXECUTANDO O DELETE
        requestDelete = RestAssured.given().header("x-api-key", "DEMO-API-KEY").
                contentType("application/json; charset=utf-8").relaxedHTTPSValidation();
    }

    @When("acessar tela inicial excluir dado inserido no Post")
    public void acessar_tela_inicial_excluir_dado_inserido_no_Post () throws Throwable {
        responseDelete = requestDelete.when().delete("favourites/" + idPost).prettyPeek();
    }

    @Then("valido a exclusao")
    public void valido_a_exclusao () throws Throwable {
        Assert.assertEquals(responseDelete.jsonPath().get("message"), "SUCCESS");

    }
}