package br.com.testeAPI.api.steps.Breeds;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.junit.Assert;

public class ValidarNotFound {

    public RequestSpecification request;
    public Response response;

    @Given("acessar API The Cat")
    public void acessarApiTheCat() {
        request  = RestAssured.given().relaxedHTTPSValidation();
    }

    @When("executar api com endereco incorreto")
    public void acessar_endereco_incorreto() throws Throwable {
         response = request.when().get("/breedss").prettyPeek();
    }
   @Then("status deve ser not found")
    public void status_deve_ser_not_found() throws Throwable {
         Assert.assertTrue(response.statusCode() == 404);
        }

    }


