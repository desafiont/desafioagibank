package br.com.testeAPI.api.steps.Favourites;


import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.junit.Assert;

public class ValidarPost {

    public RequestSpecification requestPost;
    public RequestSpecification requestDelete;
    public Response responsePost;
    public Response responseDelete;
    public Integer idPost;

    @Given("que acessei Post Favourites")
    public void que_acessei_Post_Favourites () throws Throwable {
        requestPost = RestAssured.given().header("x-api-key", "DEMO-API-KEY").
                contentType("application/json").body("{\n" +
                "  \"image_id\": \"soneira37\" ,\n" +
                "  \"sub_id\": \"your-user-1234\"\n" +
                "}").relaxedHTTPSValidation();
    }

    @When("acessar tela inicial e incluir dados")
    public void acessar_tela_inicial_e_incluir_dados () throws Throwable {
        responsePost = requestPost.when().post("/favourites").prettyPeek();
    }

    @Then("valido a inclusao")
    public void valido_a_inclusao () throws Throwable {
        Assert.assertEquals(responsePost.jsonPath().get("message"), "SUCCESS");
        idPost = responsePost.jsonPath().get("id");

        Thread.sleep(5000);

        //DELETE PARA MÃO DEIXAR REGISTRO EM BANCO
        requestDelete = RestAssured.given().header("x-api-key", "DEMO-API-KEY").
                contentType("application/json; charset=utf-8").relaxedHTTPSValidation();
        responseDelete = requestDelete.when().delete("favourites/" + idPost);

        Assert.assertEquals(responseDelete.jsonPath().get("message"), "SUCCESS");

    }
}