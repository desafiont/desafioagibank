package br.com.testeAPI.api.steps.Favourites;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.junit.Assert;
public class ValidarExcessoes {
    public RequestSpecification requestPost;
    public RequestSpecification requestDelete;
    public RequestSpecification requestGet;
    public Response responsePost;
    public Response responseDelete;
    public Response responseGet;
    public RequestSpecification request;
    public Response response;

    @Given("que acessei a API com autenticao")
    public void que_acessei_a_API_com_autenticao() throws Throwable {
        requestGet = RestAssured.given().header("x-api-key", "DEMO-API-KEY").relaxedHTTPSValidation();
    }

    @When("acessar Favourites")
    public void acessar_Favourites() throws Throwable {
        responseGet = requestGet.when().get("/favourites/990").prettyPeek();
    }

    @Then("valido os dados Favourites")
    public void valido_os_dados_Favourites() throws Throwable {
        Assert.assertEquals(responseGet.jsonPath().getInt("id"), 990);
        Assert.assertEquals(responseGet.jsonPath().get("user_id"), "4");
        Assert.assertEquals(responseGet.jsonPath().get("image_id"), "494");
        Assert.assertEquals(responseGet.jsonPath().get("sub_id"), "demo-1bb01a");
        Assert.assertEquals(responseGet.jsonPath().get("created_at"), "2018-10-27T03:56:24.000Z");
        Assert.assertEquals(responseGet.jsonPath().get("image.id"), "494");
        Assert.assertEquals(responseGet.jsonPath().get("image.url"), "https://cdn2.thecatapi.com/images/494.gif");
    }

    @When("inserir id repetido")
    public void inserir_id_repetido() throws Throwable {
        requestGet = RestAssured.given().header("x-api-key", "DEMO-API-KEY").relaxedHTTPSValidation();
        responseGet = requestGet.when().get("/favourites/1132").prettyPeek();
        if (responseGet.statusCode() == 404) {
            requestPost = RestAssured.given().header("x-api-key", "DEMO-API-KEY").
                    contentType("application/json").body("{\n" +
                    "  \"image_id\": \"MTY1ODU5Mg\" ,\n" +
                    "  \"sub_id\": \"demo-440b14\"\n" +
                    "}").relaxedHTTPSValidation();
            responsePost = requestPost.when().post("/favourites");
            requestPost = RestAssured.given().header("x-api-key", "DEMO-API-KEY").
                    contentType("application/json").body("{\n" +
                    "  \"image_id\": \"MTY1ODU5Mg\" ,\n" +
                    "  \"sub_id\": \"demo-440b14\"\n" +
                    "}").relaxedHTTPSValidation();
            responsePost = requestPost.when().post("/favourites");
        } else {
            requestPost = RestAssured.given().header("x-api-key", "DEMO-API-KEY").
                    contentType("application/json").body("{\n" +
                    "  \"image_id\": \"MTY1ODU5Mg\" ,\n" +
                    "  \"sub_id\": \"demo-440b14\"\n" +
                    "}").relaxedHTTPSValidation();
            responsePost = requestPost.when().post("/favourites");
        }
    }

    @Then("valido a mensagem Duplicate Favorite")
    public void valido_a_mensagem_Duplicate_Favorite() throws Throwable {
        Assert.assertTrue(responsePost.statusCode() == 400);
        Assert.assertEquals(responsePost.jsonPath().get("message"), "DUPLICATE_FAVOURITE - favourites are unique for account + image_id + sub_id");
    }

    @Given("que acessei Delete")
    public void que_acessei_Delete() throws Throwable {
        requestDelete = RestAssured.given().header("x-api-key", "DEMO-API-KEY").
                contentType("application/json; charset=utf-8").relaxedHTTPSValidation();
    }

    @When("acessar tela inicial excluir Favorito")
    public void acessar_tela_inicial_excluir_Favorito() throws Throwable {
        responseDelete = requestDelete.when().delete("favourites/NOK0").prettyPeek();
    }

    @Then("valido a mensagem Invalid Account")
    public void valido_a_mensagem_Invalid_Account() throws Throwable {
        Assert.assertTrue(responseDelete.statusCode() == 400);
        Assert.assertEquals(responseDelete.jsonPath().get("message"), "INVALID_ACCOUNT");
    }

    @Given("que acessei incorretamente")
    public void que_acessei_incorretamente() throws Throwable {
        request = RestAssured.given().header("x-api-key", "DEMO-API-KEY").relaxedHTTPSValidation();
    }

    @When("acessar tela inicial inserir dados incorretos")
    //GET INCORRETO
    public void acessar_tela_inicial_inserir_dados_incorretos() throws Throwable {
        response = request.when().get("/favourite/990").prettyPeek();
    }

    @Then("valido a mensagem de erro")
    public void valido_a_mensagem_de_erro() throws Throwable {
        Assert.assertTrue(response.statusCode() == 404);
    }

    @Given("que informei o acesso com body incorreto")
    public void que_informei_o_acesso() throws Throwable {
        request = RestAssured.given().header("x-api-key", "DEMO-API-KEY").
                contentType("application/json").body("{\n" +
                "  \"image\": \"soneira83\" ,\n" +
                "  \"sub\": \"your-user-1234\"\n" +
                "}").relaxedHTTPSValidation();
    }

    @When("nao sera possivel inserir dados")
    public void nao_sera_possivel_inserir_dados() throws Throwable {
        response = request.when().post("/favourites").prettyPeek();
    }

    @Then("valido o erro 400")
    public void valido_o_erro_400() throws Throwable {
        Assert.assertTrue(response.statusCode() == 400);
    }

    @When("inserir delete")
    public void inserir_delete() throws Throwable {
        response = request.when().delete("favourites/").prettyPeek();
    }

    @Then("valido o erro 405")
    public void valido_o_erro_405() throws Throwable {
        Assert.assertTrue(response.statusCode() == 405);
    }
}