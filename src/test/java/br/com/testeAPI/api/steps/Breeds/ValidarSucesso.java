package br.com.testeAPI.api.steps.Breeds;

import br.com.testeAPI.api.BaseTest;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.junit.Assert;


public class ValidarSucesso {
public RequestSpecification request;
public Response response;

    @Given("que tenho acesso a API")
    public void que_acesso_api() {
        request  = RestAssured.given().relaxedHTTPSValidation();
    }
    @When("executada API")
    public void executar() {
        response = request.when().get("/breeds").prettyPeek();
    }
    @Then("status deve ser 200")
    public void resultado_statuscode() {
            Assert.assertTrue(response.statusCode() == 200);
    }

}
