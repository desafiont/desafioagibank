package br.com.testeAPI;
import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import br.com.testeAPI.api.BaseTest;
import org.junit.runner.RunWith;
@RunWith(Cucumber.class)
@CucumberOptions(features = "src/test/resources/feature",
        plugin = {"json:reports/cucumber.json"})
public class Runner extends BaseTest {
}