@Breeds
Feature: Validar fluxos e dados da API Thecatapi - Breeds

  @Sucesso200
  Scenario: Validar execucao da API - Breeds com sucesso
    Given que tenho acesso a API
    When executada API
    Then status deve ser 200

  @Validar404
  Scenario: Validar erro de execucao da API - Breeds, quando informado endereco incorreto
    Given acessar API The Cat
    When executar api com endereco incorreto
    Then status deve ser not found

  @ValidarGetRaca
  Scenario: Validar pesquisa por raca
    Given que acessei a API Thecat
    When pesquisar pela raca abys
    Then validar os dados solicitados

  @ValidarGetRacaInexistente
  Scenario: Validar pesquisa por raca inexistente
    Given que acessei a API Thecat
    When consultar por uma raca inexistente
    Then validar status mas nao retorna dados