@Favourites
Feature: Validar fluxos e dados da API Thecatapi - Favourites

  @ValidarGetID
  Scenario: Validar pesquisa por ID
    Given que acessei a API com autenticao
    When acessar Favourites
    Then valido os dados Favourites

  @ValidarPost
  Scenario: Validar inclusao de Favorito
    Given que acessei Post Favourites
    When acessar tela inicial e incluir dados
    Then valido a inclusao

  @ValidarPostDuplicado
  Scenario: Validar inclusao de id já existente
    Given que acessei Post Favourites
    When inserir id repetido
    Then valido a mensagem Duplicate Favorite

  @ValidarDelete
  Scenario: Validar exclusao de Favorito
    Given que acessei exclusao de Favorito
    When acessar tela inicial excluir dado inserido no Post
    Then valido a exclusao

  @ValidarDeleteFavoritoInexistente
  Scenario: Validar exclusao de Favorito que nao existe
    Given que acessei Delete
    When acessar tela inicial excluir Favorito
    Then valido a mensagem Invalid Account

  @ValidarErroGet_NotFound
  Scenario: Validar falha no Get
    Given que acessei incorretamente
    When acessar tela inicial inserir dados incorretos
    Then valido a mensagem de erro

  @ValidarErroPost
  Scenario: Validar falha 400 no Post
    Given que informei o acesso com body incorreto
    When nao sera possivel inserir dados
    Then valido o erro 400

  @ValidarErroDelete
  Scenario: Validar falha 400 no Delete
    Given que informei o acesso com body incorreto
    When inserir delete
    Then valido o erro 405