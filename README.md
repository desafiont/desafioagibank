Desafio Agibank

Testes automatizados dos serviços disponibilizados pela API: https://docs.thecatapi.com/, validando os métodos GET, POST e DELETE. Não achei uma API free para utilizar o PUT, caso tenham uma para indicar, fico a disposição.
 <br><br>
Métodos utilizados:
<br><br>
GET: 
https://docs.thecatapi.com/api-reference/breeds; <br>
https://docs.thecatapi.com/api-reference/favourites/favourites-get.<br>
POST: <br>
https://docs.thecatapi.com/api-reference/favourites/favourites-post.<br>
DELETE: <br>
https://docs.thecatapi.com/api-reference/favourites/favourites-delete.<br><br>
 
Recursos utilizados: <br>

•	Rest-Assured; <br>
•	Cucumber; <br>
•	JUnit; <br>
•	Java 8; <br>
•	Gradle 6.6.1. <br><br>
 
Como configurar o ambiente:<br>

•	Faça clone do projeto: https://gitlab.com/desafiont/desafioagibank <br>
•	Importe o projeto para sua IDE de preferência; <br>
•	Faça a instalação na sua IDE do plugin 'Cucumber para Java'; <br>
•	Necessário ter instalado o JDK8 <br><br>
 
Como executar a aplicação:<br><br>
•	O projeto deve ser executado utilizando o comando: gradlew test --info <br>
•	Para gerar o relatório executar o comando: gradle generateCucumberReports<br>
•	O relatório do teste é gerado na pasta: reports/html-report<br>
•	Abrir o arquivo cucumber-html-reports/overview-features.html
